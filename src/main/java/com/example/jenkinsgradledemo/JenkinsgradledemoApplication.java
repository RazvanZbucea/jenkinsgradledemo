package com.example.jenkinsgradledemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsgradledemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsgradledemoApplication.class, args);
	}

}
